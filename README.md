# scmdmo/mupdf-android-viewer-library

Customized project based in app **mupdf-android-viewer-mini** as an Android library to use as a module and integrate a document viewer inside other apps. 
Fork from repo [git://git.ghostscript.com/mupdf-android-viewer-mini.git] [http://git.ghostscript.com/?p=mupdf-android-viewer-mini.git;a=summary] 

In order to build the underlying sources check the original [Upstream repo README](UPSTREAM_README.md)

#### To update the base project with a different release/tag use a rebase operation.
- Step 1: Add the remote (original repo that you forked) and call it “upstream”
 
    <code>git remote add upstream git://git.ghostscript.com/mupdf-android-viewer-mini.git</code>
 
- Step 2: Fetch all branches of remote upstream
 
    <code>git fetch upstream</code>
 
- Step 3: Rewrite your master with upstream’s master using git rebase.
 
    <code>git rebase upstream/master</code>
 
- Step 4: Push your updates to master. You may need to force the push with “--force”.
 
    <code>git push origin master --force</code>